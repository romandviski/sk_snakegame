// Fill out your copyright notice in the Description page of Project Settings.

#include "BonusSpeed.h"



void ABonusSpeed::BonusAction(ASnakeBase* Snake)
{
	// Скорость змейки зависит от времени тика
	// Уменьшаем время между тиками, ускоряем змейку
	Snake->ChangeSpeed(BonusPower);

	Super::BonusAction(Snake);
}
