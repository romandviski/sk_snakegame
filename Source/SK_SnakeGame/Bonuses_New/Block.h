// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BonusBase.h"

#include "Block.generated.h"

UCLASS()
class SK_SNAKEGAME_API ABlock : public ABonusBase
{
	GENERATED_BODY()

public:
	// Переопределяем родительскую функцию
	virtual void BonusAction(ASnakeBase* Snake) override;
};
