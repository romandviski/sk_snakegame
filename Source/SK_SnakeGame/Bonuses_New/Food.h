// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BonusBase.h"

#include "Food.generated.h"



UCLASS()
class SK_SNAKEGAME_API AFood : public ABonusBase
{
	GENERATED_BODY()
	
public:
	// Перезаписываем родительскую функцию
	virtual void BonusAction(ASnakeBase* Snake) override;
};
