﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interfaces/Interactable.h"
#include "Core/SnakeBase.h"

#include "BonusBase.generated.h"

// объявляем свой делегат // бывают обычные, динамические и мультикаст
// https://benui.ca/unreal/delegates-advanced/
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FBonusActivatedSignature);
//DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FBonusActivatedSignature, int32, Power)
//DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FBonusActivatedSignature, int32, Power, float, Score)
//DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FBonusActivatedSignature, int32, Power, float, Score, FName, Name)

UCLASS()
class SK_SNAKEGAME_API ABonusBase : public AActor, public IInteractable
{
	GENERATED_BODY()

public:
	ABonusBase();

	// Бонус с настраиваемой силой
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Settings", meta = (ToolTip = "Сила бонуса"))
	float BonusPower = 1.f;

	// объявляем свой делегат
	FBonusActivatedSignature BonusActivated;

protected:
	virtual void BeginPlay() override;
	
	// Переопределяем стандартный дестрой, добавляя внутрь вызов нашего делегата
	virtual void Destroyed() override;
	
	virtual void Interact(AActor* Interactor, bool bIsHead) override;
	
public:	
	virtual void BonusAction(ASnakeBase* Snake);
	
};
