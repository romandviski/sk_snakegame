﻿// Fill out your copyright notice in the Description page of Project Settings.

#include "BonusBase.h"



ABonusBase::ABonusBase()
{
	PrimaryActorTick.bCanEverTick = false;
	
}

void ABonusBase::BeginPlay()
{
	Super::BeginPlay();
	
}

void ABonusBase::Destroyed()
{
	// Наш делегат нужно вызвать раньше основного дестроя
	BonusActivated.Broadcast();
	
	Super::Destroyed();
}

void ABonusBase::Interact(AActor* Interactor, bool bIsHead)
{
	auto Snake = Cast<ASnakeBase>(Interactor);
	if (Snake)
	{
		BonusAction(Snake);
	}
}

void ABonusBase::BonusAction(ASnakeBase* Snake)
{
	// Действие бонуса определим в чаилдах
	// Общие эффекты

	Destroy();
}
