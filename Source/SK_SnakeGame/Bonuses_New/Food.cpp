// Fill out your copyright notice in the Description page of Project Settings.

#include "Food.h"

#include "Core/SnakeBase.h"



void AFood::BonusAction(ASnakeBase* Snake)
{
	// Увеличиваем змейку в зависимости от силы еды
	Snake->AddSnakeElement(BonusPower);

	// Дестрой в родителькой функции
	Super::BonusAction(Snake);
}
