﻿// Fill out your copyright notice in the Description page of Project Settings.

#include "BonusLife.h"



void ABonusLife::BonusAction(ASnakeBase* Snake)
{
	// Бонус добавляюший жизни
	Snake->ChangeLife(BonusPower);
	
	Super::BonusAction(Snake);
}
