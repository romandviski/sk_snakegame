// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

#include "Teleport.generated.h"



UCLASS()
class SK_SNAKEGAME_API ATeleport : public AActor
{
	GENERATED_BODY()
	
public:	
	ATeleport();
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components)
	class UBoxComponent* Box = nullptr;
	
	// Свои функции для их привязки на стандартные диспатчеры
	// Параметры должны совпадать с параметрами диспатчера
	UFUNCTION()
	void MyBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	UFUNCTION()
	void MyEndOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	// Просто помагает сократить количество кода
	bool bTeleportIsVertiсal = true;

	// Отступ при телепортации должен совпадать с отступом между блоками стены
	// Задаётся из спавнера
	int32 TeleportPadding = 60;
};
