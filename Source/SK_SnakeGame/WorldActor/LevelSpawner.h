﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "../Bonuses_New/BonusBase.h"
#include "../Bonuses_New/Block.h"

#include "LevelSpawner.generated.h"



UCLASS()
class SK_SNAKEGAME_API ALevelSpawner : public AActor
{
	GENERATED_BODY()

public:
	ALevelSpawner();

protected:
	virtual void BeginPlay() override;
	
	// Массив локейшенов в рамках поля
	TArray<FVector> LocationForBonus;
	
public:
	// Назначаем блюпринты для спавна
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TSubclassOf<AActor> FloorActor;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TSubclassOf<AActor> WallActor;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TSubclassOf<ABlock> BlockActor;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<TSubclassOf<ABonusBase>> BonusTypeArray;

	// Настройки спавнера
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 SizeX = 5;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 SizeY = 5;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 Padding = 60;
	
	void SpawnFloor();
	void SpawnWallsAndTeleports();
	void SpawnBlock();

	UFUNCTION() // Нужен пустой UFUNCTION для корректной работы с делегатом
	void SpawnBonus();
};
