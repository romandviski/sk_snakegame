// Fill out your copyright notice in the Description page of Project Settings.

#include "Teleport.h"

#include "Components/BoxComponent.h"



ATeleport::ATeleport()
{
	PrimaryActorTick.bCanEverTick = false;

	// Создаю и настраиваю простой коллижн бокс
	Box = CreateDefaultSubobject<UBoxComponent>(TEXT("My Box Collision"));
	Box->SetupAttachment(RootComponent);
	Box->SetBoxExtent(FVector(50,50,50), true);
	Box->SetRelativeScale3D(FVector(0.5,0.5,0.5));

	// Привязываем наши функции к делегату внутри компонента
	Box->OnComponentBeginOverlap.AddDynamic(this, &ATeleport::MyBeginOverlap);
	Box->OnComponentEndOverlap.AddDynamic(this, &ATeleport::MyEndOverlap);
}

void ATeleport::MyBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp,
	int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	// Телепортирует на противоположную сторону поля, с учётом паддинга
	FVector ExitLocation = GetActorLocation();
	
	if (bTeleportIsVertiсal)
	{
		// Длинный пример на основе обычного if
		// Ниже на основе тернарной операции
		if (ExitLocation.Y > 0)
		{
			ExitLocation.Y = (ExitLocation.Y - TeleportPadding) * -1;
		}
		else
		{
			ExitLocation.Y = (ExitLocation.Y + TeleportPadding) * -1;
		}
	}
	else
	{
		// очень сильно можно сократить тернарными операциями
		// пример тернарной операции int a = b > 0 ? 1 : -1;
		ExitLocation.X = (ExitLocation.X > 0 ? ExitLocation.X - TeleportPadding : ExitLocation.X + TeleportPadding) * -1;
	}

	// Перемещаю вошедшего актора в нужную локацию
	OtherActor->SetActorLocation(ExitLocation, false, nullptr, ETeleportType::None);
}

void ATeleport::MyEndOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp,
	int32 OtherBodyIndex)
{
	// Не интересует нас сейчас, просто на память
}
