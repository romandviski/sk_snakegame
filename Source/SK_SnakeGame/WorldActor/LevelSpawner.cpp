﻿// Fill out your copyright notice in the Description page of Project Settings.

#include "LevelSpawner.h"

#include "Teleport.h"
#include "Kismet/GameplayStatics.h"
#include "Core/SnakeGameModeBase.h"



ALevelSpawner::ALevelSpawner()
{
	PrimaryActorTick.bCanEverTick = false;

}

void ALevelSpawner::BeginPlay()
{
	Super::BeginPlay();

	SpawnFloor();
	SpawnWallsAndTeleports();
	SpawnBlock();
	SpawnBonus();
}

void ALevelSpawner::SpawnFloor()
{
	// Спавним ячейки пола вокруг локального нуля
	// Тоже самое что делали в арканойде
	const FVector Location = GetActorLocation();
	const FVector CenterOffset = FVector((SizeX - 1) * Padding * 0.5f, (SizeY - 1) * Padding * 0.5f, 0.f);
	
	for (int32 i = 0; i < SizeX; i++)
	{
		for (int32 j = 0; j < SizeY; j++)
		{
			// Спавн кубиков пола, если сетка на полу не нужна, то и пол проще руками поставить на карту
			FVector SpawnLocation = Location + FVector(Padding * i, Padding * j, -60.f) - CenterOffset;
			GetWorld()->SpawnActor<AActor>(FloorActor, FTransform(SpawnLocation));
		}
	}
}

void ALevelSpawner::SpawnWallsAndTeleports()
{
	// спавним стены, телепорты и собираем локейшн свободных ячеек для бонусов
	FVector Location = GetActorLocation();
	FVector CenterOffset = FVector((SizeX - 1) * Padding * 0.5f, (SizeY - 1) * Padding * 0.5f, 0.f);
	
	for (int32 i = 0; i < SizeX; i++)
	{
		for (int32 j = 0; j < SizeY; j++)
		{
			if (i == 0 || i == SizeX - 1)
			{
				FVector SpawnLocation = Location + FVector(Padding * i, Padding * j, 0.f) - CenterOffset;
				GetWorld()->SpawnActor<AActor>(WallActor, FTransform(SpawnLocation));
				auto CurrentTeleport = GetWorld()->SpawnActor<ATeleport>(ATeleport::StaticClass(), FTransform(SpawnLocation));
				CurrentTeleport->bTeleportIsVertiсal = false; // Телепорт размещён по горизонтали
				CurrentTeleport->TeleportPadding = Padding; // нарушение ООП!!!!!
			}
			else if (j == 0 || j == SizeY - 1)
			{
				FVector SpawnLocation = Location + FVector(Padding * i, Padding * j, 0.f) - CenterOffset;
				GetWorld()->SpawnActor<AActor>(WallActor, FTransform(SpawnLocation));
				auto CurrentTeleport = GetWorld()->SpawnActor<ATeleport>(ATeleport::StaticClass(), FTransform(SpawnLocation));
				CurrentTeleport->bTeleportIsVertiсal = true; // Телепорт размещён по вертикали
				CurrentTeleport->TeleportPadding = Padding;
			}
			else
			{
				// Все ячейки в центре не занятые стенами собираем в массив для спавна бонусов
				FVector SpawnLocation = Location + FVector(Padding * i, Padding * j, 0.f) - CenterOffset;
				LocationForBonus.Add(SpawnLocation);
			}
		}
	}
}

void ALevelSpawner::SpawnBlock()
{
	// спавним несколько припятствий на уровне
	for (int32 i = 0; i < (SizeY/2); i++)
	{
		int32 RandomLocationIndex = FMath::RandRange(0,LocationForBonus.Num() - 1);
		auto SpawnLocation = LocationForBonus[RandomLocationIndex];
		LocationForBonus.RemoveAt(RandomLocationIndex);
		
		GetWorld()->SpawnActor<ABlock>(BlockActor, FTransform(SpawnLocation));
	}
}

void ALevelSpawner::SpawnBonus()
{
	if (BonusTypeArray.Num()) // если массив бонусов не пустой
	{
		// выбираем рандомный бонус и локацию
		int32 RandomBonusIndex = FMath::RandRange(0,BonusTypeArray.Num() - 1);
		auto ClassForSpawn = BonusTypeArray[RandomBonusIndex];
		int32 RandomLocationIndex = FMath::RandRange(0,LocationForBonus.Num() - 1);
		auto SpawnLocation = LocationForBonus[RandomLocationIndex];

		// спавним бонус
		auto CurrentBonus = GetWorld()->SpawnActor<ABonusBase>(ClassForSpawn, FTransform(SpawnLocation));

		// Подвязываюю на активацию бонуса спавн нового бонуса
		CurrentBonus->BonusActivated.AddDynamic(this, &ALevelSpawner::SpawnBonus);
		// Устанавливаю время жизни бонуса
		CurrentBonus->SetLifeSpan(10.0f);

		// Полезные комманды для дебага
		// FString DebugMessage = TEXT("Bonus name: ") + CurrentBonus->GetName();
		// GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Yellow, *DebugMessage);
	}
}
