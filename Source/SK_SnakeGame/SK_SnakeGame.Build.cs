
using UnrealBuildTool;

public class SK_SnakeGame : ModuleRules
{
	public SK_SnakeGame(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;
	
		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore" });

		// Чиним инклюды из подпапок проекта
		PublicIncludePaths.AddRange(new string[]
		{
			"SK_SnakeGame"
		});
	}
}
