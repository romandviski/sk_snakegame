// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "../Interfaces/Interactable.h"

#include "Bonus.generated.h"


// Старый универсальный бонус, был порождён ленью =)
UCLASS()
class SK_SNAKEGAME_API ABonus : public AActor, public IInteractable
{
	GENERATED_BODY()
	
public:	
	ABonus();

	// Булевые определяют тип бонуса
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool BnSpeed;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool BnLife;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float BnPower;

protected:
	virtual void BeginPlay() override;

public:	
	virtual void Tick(float DeltaTime) override;

	virtual void Interact(AActor* Interactor, bool bIsHead) override;
};
