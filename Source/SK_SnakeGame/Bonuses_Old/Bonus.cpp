// Fill out your copyright notice in the Description page of Project Settings.

#include "Bonus.h"

#include "Core/SnakeBase.h"



ABonus::ABonus()
{
	PrimaryActorTick.bCanEverTick = true;
	BnSpeed = false;
	BnLife = false;
	BnPower = 0.1f;
}

void ABonus::BeginPlay()
{
	Super::BeginPlay();
	
}

void ABonus::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABonus::Interact(AActor * Interactor, bool bIsHead)
{
	auto Snake = Cast<ASnakeBase>(Interactor);
	if (IsValid(Snake))
	{
		// Бонус может и кормить змейку и увеличивать одновременно
		if (BnLife == true)
		{
			Snake->Life += BnPower;
		}

		if (BnSpeed == true)
		{
			Snake->SetActorTickInterval(Snake->MovementSpeed -= BnPower);
		}

		// Если нет спавнера, можно переставлять одну и туже еду
		//int RandomX = FMath::RandRange(-380, 450); // Рандомим X
		//int RandomY = FMath::RandRange(-900, 900); // Рандомим Y
		//SetActorLocation(FVector(RandomX, RandomY, 0)); // Устанавливаем новый локейшн
		
		Destroy();
	}
}
