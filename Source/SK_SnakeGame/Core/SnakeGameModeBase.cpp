// Fill out your copyright notice in the Description page of Project Settings.

#include "SnakeGameModeBase.h"



ASnakeGameModeBase::ASnakeGameModeBase()
{
	PrimaryActorTick.bCanEverTick = false;
	
}

void ASnakeGameModeBase::BeginPlay()
{
	Super::BeginPlay();

	// Спавн перенесён в отдельного актора LevelSpawner <--------------------------------------------------
	bool bSpawnTimer = false;
	if(bSpawnTimer)
	{
		// Объявляем таймер
		FTimerHandle SpawnTimer;
		
		// Заводим таймер для спавна
		GetWorld()->GetTimerManager().SetTimer(SpawnTimer, this, &ASnakeGameModeBase::SpawnSomething, 10.0f, true);
	}
}

// Функция для спавна рандомных классов из массива
void ASnakeGameModeBase::SpawnSomething()
{
	if (SpawnActorArray.Num())
	{
		int32 RandomIndex = FMath::RandRange(0,SpawnActorArray.Num() - 1);
		auto ClassForSpawn = SpawnActorArray[RandomIndex];

		// Получаем случайные координаты в пределах карты
		int32 RandomX = FMath::RandRange(-420, 420);
		int32 RandomY = FMath::RandRange(-900, 900);

		// Спавн рандомного бонуса из массива
		auto CurrentBonus = GetWorld()->SpawnActor<ABonusBase>(ClassForSpawn, FTransform(FVector(RandomX, RandomY, 0)));

		// Ограничиваем время жизни бонуса
		CurrentBonus->SetLifeSpan(20.0f);

		// Отчитываемся в лог что бонус был спавнен (для строки нужен именно указатель)
		UE_LOG(LogTemp, Log, TEXT("Spawned %s"), *ClassForSpawn->GetName());
	}
}
