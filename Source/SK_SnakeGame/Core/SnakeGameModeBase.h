// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
//#include "SK_SnakeGame/Bonuses/BonusBase.h"
#include "Bonuses_New/BonusBase.h"

#include "SnakeGameModeBase.generated.h"



UCLASS()
class SK_SNAKEGAME_API ASnakeGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

public:
	ASnakeGameModeBase();

	// Массив для заполнения в блюпринте и спавна по таймеру
	UPROPERTY(EditDefaultsOnly)
	TArray<TSubclassOf<ABonusBase>> SpawnActorArray;
	
protected:
	virtual void BeginPlay() override;
	
public:
	// Функция для спавна
	void SpawnSomething();
};
